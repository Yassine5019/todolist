<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ItemController extends Controller
{
   public function index(){
     return Item::orderBy('created_at', 'DESC')->get();
   }

   public function store(Request $request){
     $newItem=new Item();
     $newItem->name=$request->item["name"];
     $newItem->save();
     return $newItem;
   }

   public function update(Request $request, $id){
    $newItem=Item::find( $id );
        if($newItem){
            $newItem->completed=$request->item["completed"] ? true : false;
            $newItem->completed_at=$request->item["completed"] ? Carbon::now() : null;
            $newItem->save();
            return $newItem;
        }
     return "This item not found.";
   }

   public function delete($id){
    $newItem=Item::find( $id );
        if($newItem){
            $newItem->delete();
            return "this Item has deleted";
        }
     return "This item not found.";
   }
}
